//
//  FileOfConstants.swift
//  Login App
//
//  Created by apple on 18/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import Foundation
//ALl the constants of view controller...
let permissionConstants = ["public_profile", "email", "user_friends", "publish_stream","user_photos","publish_actions"]
let printLogin = "User Logged In"
let photographs = "photos"
let post = "POST"
let alertMessage = "Picture/status Uploaded"
let alert2 = "Please enter a status or load an image"
let alertConst = "Alert"
let logoutMessage = "User Logged Out"
let picture = "picture"
let ok = "OK"
let error = "Error:"
let status = "status"
let posts = "POST"
let id = "id"
let nilValue = ""

//For mobile use only(refrence)
// access the camera of the device
/*func openCamera() {
// if the device has camera provision open it otherwise open gallery
if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)) {
picker!.sourceType = UIImagePickerControllerSourceType.Camera
self .presentViewController(picker!, animated: true, completion: nil)
} else {
openGallary()
}
}*/
