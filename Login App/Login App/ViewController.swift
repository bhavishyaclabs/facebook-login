//
//  ViewController.swift
//  Login App
//
//  Created by apple on 10/03/15.
//  Copyright (c) 2015 Bhavishya. All rights reserved.
//

import UIKit

class ViewController: UIViewController, FBLoginViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIPopoverControllerDelegate,UITextFieldDelegate {
    
    var status = nilValue
    var pickedImage = UIImage()
    var counter = 0
    
    @IBOutlet var statusField: UITextField!
    @IBOutlet var openGallery: UIButton!
    @IBOutlet var imageView: UIImageView!
    // Created an instance of picker controller
    var picker:UIImagePickerController? = UIImagePickerController()
    var popover:UIPopoverController? = nil
    //login page...
    var fbl: FBLoginView = FBLoginView()
    
    @IBOutlet var fblonView: FBLoginView!
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        fblonView.delegate = self
        //Permissions...
        self.fblonView.readPermissions = permissionConstants
        
        // clear the caption at view load
        statusField.text = nilValue
        picker?.delegate = self
        statusField.delegate = self
    }
    
    // function called when a user is logged in
    func loginViewShowingLoggedInUser(loginView : FBLoginView!) {
        println(printLogin)
    }
    
    // show the details of logged in user
    func loginViewFetchedUserInfo(loginView : FBLoginView!, user: FBGraphUser) {
        println("User: \(user)")
        println("User ID: \(user.objectID)")
        println("User Name: \(user.name)")
        var userEmail = user.objectForKey("email") as String
        println("User Email: \(userEmail)")
        
        var params : NSMutableDictionary = NSMutableDictionary()
        FBRequestConnection.startWithGraphPath(photographs, parameters: params, HTTPMethod: post, completionHandler: {
            (connection,result,error) in
        })
    }
    
    @IBAction func shareButton(sender: AnyObject) {
        var image : UIImage = pickedImage
        var photoId = nilValue
        
        if statusField.text != nilValue || imageView.image != nilValue {
            // counter checks tht the image is uploaded only once
            if counter == 0 {
                // parameters of the image to be uploaded
                var params : NSMutableDictionary = NSMutableDictionary(objectsAndKeys: "\(status)", status,image, picture)
                
                // call the facebook graph to post the image
                FBRequestConnection.startWithGraphPath(photographs, parameters: params, HTTPMethod: posts, completionHandler: {
                    (connection,result,error) in
                    
                    if self.imageView.image != nil && result != nil {
                        photoId = result.objectForKey(id) as String
                        println(photoId)
                        println(self.statusField.text)
                        //gives an alert message when item added...
                        var alert = UIAlertController(title: alertConst, message: alertMessage, preferredStyle: UIAlertControllerStyle.Alert)
                        alert.addAction(UIAlertAction(title: ok , style: UIAlertActionStyle.Default, handler: nil))
                        self.presentViewController(alert, animated: true, completion: nil)
                    }
                    //sets to nil...
                    self.statusField.text = nilValue
                    self.imageView.image = nil
                })
            }
        } else {
            var alert = UIAlertController(title: alertConst, message: alert2 , preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: ok , style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    // called when a user is logged out
    func loginViewShowingLoggedOutUser(loginView : FBLoginView!) {
        println(logoutMessage)
    }
    
    // called when there is any kind of error during login process
    func loginView(loginView : FBLoginView!, handleError:NSError) {
        println(error + " \(handleError.localizedDescription)")
    }
    
    @IBAction func openGalleryClicked(sender: AnyObject) {
        self.openGallary()
    }
    
    // access the gallery on the device
    func openGallary() {
        picker!.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            self.presentViewController(picker!, animated: true, completion: nil)
        } else {
            popover=UIPopoverController(contentViewController: picker!)
            popover!.presentPopoverFromRect(openGallery.frame, inView: self.view, permittedArrowDirections: UIPopoverArrowDirection.Any, animated: true)
        }
    }
    
    // what should be done when an image has been selected
    func imagePickerController(picker: UIImagePickerController!, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]!) {
        // save the image in the global variable and display the selected image
        picker .dismissViewControllerAnimated(true, completion: nil)
        pickedImage  = info[UIImagePickerControllerOriginalImage] as UIImage
        imageView.image = pickedImage
    }
    
    override func supportedInterfaceOrientations() -> Int {
        return Int(UIInterfaceOrientationMask.Portrait.rawValue)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        statusField.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
